https://robocode.sourceforge.io/

**<h1>Robô: Fast</h1>**

Saudações,

Este robô se mavimenta bem (ao meu ver), tenta ao maximo se desvinciliar de ataques, seu mecanismo de mira insiste em um oponente enquanto o robô não esta sendo atingido, há diferença de potencia de tiro em relação ao quão distante está o seu oponente.

**Seus pontos fortes**


- Se movimenta de forma a se desvinciliar de tiros
- Seu mecanismo de mira que insiste em determinado oponende
- caso encoste nas bordas existe função para sair do mesmo

**Seus pontos fracos**

- caso o oponente esteja se movimentando muito, é gasto muitos tiros para atingilo de forma eficiênte
- o robô encosta nas bordas
- se o oponente estiver lonje e se movimenta de forma aleatoria e gasto muitos tiros o que pode acarretar em falta de energia


Não conhecia este jogo e achei muito interessante e didatico, com certeza irei continuar aprendendo para aperfeiçoá-lo cada vez mais. agradeço a oportunidade e espero conhecer vocês da soluts.
 