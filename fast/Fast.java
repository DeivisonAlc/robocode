package fast;
import robocode.*;
import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Fast - a robot by (Deivison_Alcantara)
 */
public class Fast extends AdvancedRobot
{
	int i = 0;
	boolean p = false;
	//double distancia_inimigo;



	public void run() {

		setScanColor(Color.yellow);
		setRadarColor(Color.yellow);
		
		while(true) {

			setAhead(100);
			setTurnGunRight(15);
			execute();	
		}
	}

	/**
	 * onScannedRobot: O que fazer quando você vê outro robô
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		mira(e.getBearing());

		if(e.getEnergy() < 12) {
			tiroDaMorte(e.getEnergy());
		}
		
		if (e.getDistance() < 50){
			setFire(3);
		}
		if (e.getDistance() < 200){
			setFire(2);
		}
		else {
			setFire(1);
		}
		scan();

	}
	
	/**
	 * onHitByBullet: o que fazer quando atingido
	 */

	public void onHitByBullet(HitByBulletEvent e) {
		
		if(i==0){
		setTurnRight(180);
		setAhead(200);
		i = 1;
		}
		else{
			setTurnLeft(180);
			i = 0;
		}
		setTurnGunRight(360);
	}
	
	/**
	 * onHitWall: bater na borda
	 */
	public void onHitWall(HitWallEvent e) {
		
		setTurnRight(90);	
	}

	/**
	*  onHitRobot: bater em outro robô
	*/
	
	public void onHitRobot(HitWallEvent e) {
		
		mira(e.getBearing());
		turnRight(e.getBearing()); 
		setFire(3);
	}
	
		public void mira(double Adv) {
		double A=getHeading()+Adv-getGunHeading();
		if (!(A > -180 && A <= 180)) {
			while (A <= -180) {
				A += 360;
			}
			while (A > 180) {
				A -= 360;
			}
		}
		setTurnGunRight(A);
	}
	/**
	*  onWin: Perfumaria
	*/
	public void onWin(WinEvent event){
		setMaxVelocity(5);
		setTurnGunRight(10000);
		while(true) {
			ahead(20);
			back(20);  
			if (getEnergy() > 0.1) {
				fire(0.1);
			}
		}
	}
	/**
	 * tiroDaMorte: calculo que analiza a energia do inimigo e faz um tiro mais forte
	 */
	public void tiroDaMorte(double EnergiaIni) {
		double Tiro = (EnergiaIni / 4) + .1;
		fire(Tiro);
	}
}
